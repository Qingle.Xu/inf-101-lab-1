package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        multiplesOfSevenUpTo(49);

        multiplicationTable(3);
        multiplicationTable(5);

        int sum = crossSum(1);
        System.out.println(sum); // 1

        int sum2 = crossSum(12);
        System.out.println(sum2); // 3

        int sum3 = crossSum(123);
        System.out.println(sum3); // 6

        int sum4 = crossSum(1234);
        System.out.println(sum4); // 10

        int sum5 = crossSum(4321);
        System.out.println(sum5); // 10

    }

    public static void multiplesOfSevenUpTo(int n) {
        for(int i = 1; i <= n; i++) {
            if (i % 7 == 0) {
                System.out.println(i);
            }
        }
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i <= n; i++) {
            System.out.print(i + ": ");

            for(int j = 1; j <= n; j++) {
                System.out.print(i * j + " ");
            }
            System.out.println();

        }
    }

    public static int crossSum(int num) {
        int total = 0;
        while (num != 0) {            
            int remainder = num % 10;
            total += remainder;
            num = Math.floorDiv(num, 10);
        }
        return total;

     }

}