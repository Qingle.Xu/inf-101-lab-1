package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
    
        
        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(3, 0, 9)));
        grid1.add(new ArrayList<>(Arrays.asList(4, 5, 3)));
        grid1.add(new ArrayList<>(Arrays.asList(6, 8, 1)));
        
        boolean equalSums1 = allRowsAndColsAreEqualSum(grid1);
        System.out.println(equalSums1); // false
  

        ArrayList<ArrayList<Integer>> grid2 = new ArrayList<>();
        grid2.add(new ArrayList<>(Arrays.asList(3, 4, 6)));
        grid2.add(new ArrayList<>(Arrays.asList(0, 5, 8)));
        grid2.add(new ArrayList<>(Arrays.asList(9, 3, 1)));


        boolean equalSums2 = allRowsAndColsAreEqualSum(grid2);
        System.out.println(equalSums2); // false


        ArrayList<ArrayList<Integer>> grid3 = new ArrayList<>();
        grid3.add(new ArrayList<>(Arrays.asList(1, 2, 3, 4)));
        grid3.add(new ArrayList<>(Arrays.asList(2, 3, 4, 1)));
        grid3.add(new ArrayList<>(Arrays.asList(3, 4, 1, 2)));
        grid3.add(new ArrayList<>(Arrays.asList(4, 1, 2, 3)));

        boolean equalSums3 = allRowsAndColsAreEqualSum(grid3);
        System.out.println(equalSums3); // true

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);

    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        if (checkRowsAreEqualSum(grid) && checkColsAreEqualSum(grid)) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean checkRowsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        List<Integer> total = new ArrayList<>();
        for (ArrayList<Integer> grid2 : grid) {
            int sum = 0;
            for (Integer element : grid2) {
                sum += element;
            }
            total.add(sum);
        }

        for (Integer total2 : total) {
            if (!total2.equals(total.get(0))) {
                return false;
            }
        }
        return true;

    }
    public static boolean checkColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        List<Integer> totalCol = new ArrayList<>();

        int sum = 0;
        int j = 0;
        
        while (true) {
            for (int i = 0; i < grid.size(); i++) {// grid.size() means number of rows of grid

                //for (int k = j; k < grid.get(i).size();k++) { // grid.get(0).size() means number of columns of grid
                sum += grid.get(i).get(j);
                    
                //}
            }

            totalCol.add(sum);
            sum = 0;
            j++;
            
            if (j == grid.size()) {
                break;
            }
        }
        
        for (Integer total2 : totalCol) {
            if (!total2.equals(totalCol.get(0))) {
                return false;
            }
        }
        return true;

    }

}