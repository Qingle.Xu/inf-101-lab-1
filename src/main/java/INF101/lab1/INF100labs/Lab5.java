package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        /**
        List<Integer> list1 = Arrays.asList(1, 2, 3, 4);
        ArrayList<Integer> arrayList1 = new ArrayList<>(list1);
        removeThrees(arrayList1);
        System.out.println(arrayList1); // [1, 2, 4]

        List<Integer> list2 = Arrays.asList(1, 2, 3, 3);
        ArrayList<Integer> arrayList2 = new ArrayList<>(list2);
        removeThrees(arrayList2);
        System.out.println(arrayList2); // [1, 2]

        List<Integer> list3 = Arrays.asList(3, 3, 1, 3, 2, 4, 3, 3, 3);
        ArrayList<Integer> arrayList3 = new ArrayList<>(list3);
        removeThrees(arrayList3);
        System.out.println(arrayList3); // [1, 2, 4]

        List<Integer> list4 = Arrays.asList(3, 3);
        ArrayList<Integer> arrayList4 = new ArrayList<>(list4);
        removeThrees(arrayList4);
        System.out.println(arrayList4); // [3, 3]

         */
        
        List<Integer> list1 = Arrays.asList(1, 1, 2, 1, 3, 3, 3, 2);
        ArrayList<Integer> arrayList1 = new ArrayList<>(list1);
        uniqueValues(arrayList1);
        System.out.println(arrayList1); // [1, 2, 3]
        
        List<Integer> list2 = Arrays.asList(4, 4, 4, 4, 4, 4, 4, 4, 4, 5);
        ArrayList<Integer> arrayList2 = new ArrayList<>(list2);
        uniqueValues(arrayList2);
        System.out.println(arrayList2); // [4, 5]

        ArrayList<Integer> a1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> b1 = new ArrayList<>(Arrays.asList(4, 2, -3));
        addList(a1, b1);
        System.out.println(a1); // [5, 4, 0]

        ArrayList<Integer> a2 = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> b2 = new ArrayList<>(Arrays.asList(47, 21, -30));
        addList(a2, b2);
        System.out.println(a2); // [48, 23, -27]


    }
    // tutorialspoint https://www.tutorialspoint.com/how-do-i-remove-multiple-elements-from-a-list-in-java 16/01/2023
    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        List<Integer> list1 = new ArrayList<>(Arrays.asList(3));
        list.removeAll(list1);
        return list;
    }
    //geeksforgeeks https://www.geeksforgeeks.org/how-to-remove-duplicates-from-arraylist-in-java/ 16/01/2023
    public static List<Integer> uniqueValues(ArrayList<Integer> list) {
        List<Integer> newList = new ArrayList<Integer>();
        for (int i: list) {
            if(!newList.contains(i)) {
                newList.add(i);
            }
        }
        list.clear();
        for(int element : newList) {
            list.add(element);
        }
        return list;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < a.size(); i++) {
            int tempOfA = a.get(i);
            int tempOfB = b.get(i);
            int total = tempOfA + tempOfB;
            a.set(i, total);
        }
    }

}