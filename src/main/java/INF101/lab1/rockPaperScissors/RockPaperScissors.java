package INF101.lab1.rockPaperScissors;

import java.util.List;
import java.util.Arrays;
import java.util.Scanner;

public class RockPaperScissors {
	
    public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true) {
            System.out.println("Let's play round " + roundCounter);

            //Humand and Computer choice
            String humanChoice = userChoice();
            String computerChoice = randomChoice(rpsChoices);
            String choiceString = "Human chose " + humanChoice + ", computer chose "+ computerChoice + ".";
            
            // Check who won
            if (isWinner(humanChoice, computerChoice)) {
                System.out.println(choiceString + " Human wins.");
                humanScore++;
            } else if (isWinner(computerChoice, humanChoice)) {
                System.out.println(choiceString + " Computer wins.");
                computerScore++;
            } else{
                System.out.println(choiceString + " It's a tie");
            }
            
            System.out.println("Score: me " + humanScore + " you "+ computerScore);
    
            // Ask if human wants to play again
            String continue_answer = continue_playing();
            if (continue_answer.equals("n")) {
                break;
            } else {
                roundCounter++;
            }
        }
        System.out.println("Bye bye :)");

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    /**
     * Return random choice from a give string arrayList
     * @param list
     * @return random element of arrayList
     */
    public String randomChoice(List<String> list) {
        int i = (int) (Math.random() * list.size());
        return list.get(i);
    }

    /**
     * Check winner with given choice
     * @param choice1
     * @param choice2
     * @return true if choice1 winner else false
     */
    public boolean isWinner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return choice2.equals("rock");
        } else if (choice1.equals("scissors")) {
            return choice2.equals("paper");
        } else{
            return choice2.equals("scissors");
        }

    }

    public  String userChoice() {
        while (true) {
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (validateInput(humanChoice, rpsChoices)) {
                return humanChoice;
            }
            else {
                System.out.println("I don't understand " + humanChoice + " Try again");
            }
        }
    }

    public  String continue_playing() {
        while (true) {
            String continueAnswer = readInput("Continue (y/n)?").toLowerCase();
            List<String> yesOrNo = Arrays.asList("y", "n");

            if (validateInput(continueAnswer, yesOrNo)) {
                return continueAnswer;
            } else {
                System.out.println("I don't understand " + continueAnswer + " Try again");
            }
        }
    }

    public static boolean validateInput(String input, List<String> validInput) {
        input = input.toLowerCase();
        return validInput.contains(input);
    }
}